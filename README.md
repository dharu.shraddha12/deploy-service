# Introduction 
Release pipeline to deploy resources  in all environment

# Getting Started

1. Installation process =>
  a.Clone the given Azure Repository
  b.Change infrastructure congiguration based on your infrastructure 
    example : azure.pipeline - modify  your subscription name and id 
              "template/parameters/env" - verify or modify service the parameters like keyvault reference id in "sqldb,json", azure repo details in "datafactory.json"
  c. after all valid configuratin project is ready to deploy

2.	Software dependencies => Azure DevOps , Azure portal in same domain

3. Execution Steps: 

a. Run “azure-pipelines.yml” in Azure Repository
b. Select environment of deployment
c. Select valid parameters for region, Subscription and subscription Id
d. Specify resource group name to deploy resources and Run

